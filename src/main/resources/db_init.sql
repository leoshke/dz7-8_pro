CREATE TABLE roles(
                      id BIGSERIAL PRIMARY KEY,
                      title VARCHAR,
                      description VARCHAR
);
CREATE TABLE users(
                      id BIGSERIAL PRIMARY KEY,
                      login VARCHAR,
                      password VARCHAR,
                      first_name VARCHAR,
                      last_name VARCHAR,
                      middle_name VARCHAR,
                      birth_date DATE,
                      phone VARCHAR,
                      address VARCHAR,
                      email VARCHAR,
                      created_when TIMESTAMP,
                      role_id BIGSERIAL REFERENCES roles(id)
);
CREATE TABLE films(
                      id BIGSERIAL PRIMARY KEY,
                      title VARCHAR NOT NULL ,
                      premier_year SMALLINT NOT NULL ,
                      country VARCHAR NOT NULL ,
                      genre VARCHAR NOT NULL
);
CREATE TABLE orders(
                       id BIGSERIAL PRIMARY KEY,
                       user_id BIGSERIAL REFERENCES users(id),
                       film_id BIGSERIAL REFERENCES films(id),
                       rent_date DATE,
                       rent_period VARCHAR,
                       purchase boolean
);
CREATE TABLE directors(
                          id BIGSERIAL PRIMARY KEY,
                          directors_fio VARCHAR,
                          position VARCHAR
);
CREATE TABLE films_directors(
    film_id BIGSERIAL REFERENCES films(id),
    director_id BIGSERIAL REFERENCES directors(id)
);