package com.example.dz78_pro.repository;

import com.example.dz78_pro.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
