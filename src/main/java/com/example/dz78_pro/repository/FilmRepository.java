package com.example.dz78_pro.repository;

import com.example.dz78_pro.entity.Film;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmRepository extends JpaRepository<Film, Long> {
}
