package com.example.dz78_pro.repository;

import com.example.dz78_pro.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
