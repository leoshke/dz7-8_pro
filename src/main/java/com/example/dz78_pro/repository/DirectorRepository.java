package com.example.dz78_pro.repository;

import com.example.dz78_pro.entity.Director;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DirectorRepository extends JpaRepository<Director, Long> {
}
