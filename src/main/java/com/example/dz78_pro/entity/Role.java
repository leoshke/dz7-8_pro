package com.example.dz78_pro.entity;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;


@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Hidden
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;

    public Role(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Role() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
