package com.example.dz78_pro.entity;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDate;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Hidden
    private long id;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @Hidden
    private User user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "film_id")
    @Hidden
    private Film film;
    @Column(name = "rentDate")
    @CreationTimestamp
    @Hidden
    private LocalDate rentDate;
    @Column(name = "rentPeriod")
    private String rentPeriod;
    @Column(name = "purchase")
    private boolean purchase;

    public Order(User user, Film film, LocalDate rentDate, String rentPeriod, boolean purchase) {
        this.user = user;
        this.film = film;
        this.rentDate = rentDate;
        this.rentPeriod = rentPeriod;
        this.purchase = purchase;
    }

    public Order() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public LocalDate getRentDate() {
        return rentDate;
    }

    public void setRentDate(LocalDate rentDate) {
        this.rentDate = rentDate;
    }

    public String getRentPeriod() {
        return rentPeriod;
    }

    public void setRentPeriod(String rentPeriod) {
        this.rentPeriod = rentPeriod;
    }

    public boolean isPurchase() {
        return purchase;
    }

    public void setPurchase(boolean purchase) {
        this.purchase = purchase;
    }
}
