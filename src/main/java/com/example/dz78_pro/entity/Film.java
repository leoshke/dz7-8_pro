package com.example.dz78_pro.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "films")
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Hidden
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "premier_year")
    private int premierYear;
    @Column(name = "country")
    private String country;
    @Column(name = "genre")
    private String genre;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "films_directors",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "director_id"))
    @Hidden
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    private List<Director> filmDirectors;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPremierYear() {
        return premierYear;
    }

    public void setPremierYear(int premierYear) {
        this.premierYear = premierYear;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public List<Director> getFilmDirectors() {
        return filmDirectors;
    }

    public void setFilmDirectors(List<Director> filmDirectors) {
        this.filmDirectors = filmDirectors;
    }

    public void addDirector(Director director) {
        filmDirectors.add(director);
    }

    public Film(String title, int premierYear, String country, String genre, List<Director> filmDirectors) {
        this.title = title;
        this.premierYear = premierYear;
        this.country = country;
        this.genre = genre;
        this.filmDirectors = filmDirectors;
    }

    public Film() {
    }
}
