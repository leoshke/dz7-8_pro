package com.example.dz78_pro.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "directors")
public class Director {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Hidden
    private long id;
    @Column(name = "directors_fio")
    private String directorsFio;
    @Column(name = "position")
    private String position;
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "filmDirectors", fetch = FetchType.LAZY)
    @Hidden
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    private List<Film> directorFilms;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDirectorsFio() {
        return directorsFio;
    }

    public void setDirectorsFio(String directorsFio) {
        this.directorsFio = directorsFio;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public List<Film> getDirectorFilms() {
        return directorFilms;
    }

    public void setDirectorFilms(List<Film> directorFilms) {
        this.directorFilms = directorFilms;
    }

    public void addFilm(Film film) {
        directorFilms.add(film);
    }

    public Director() {
    }

    public Director(String directorsFio, String position, List<Film> directorFilms) {
        this.directorsFio = directorsFio;
        this.position = position;
        this.directorFilms = directorFilms;
    }
}
