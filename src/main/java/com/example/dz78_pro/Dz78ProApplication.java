package com.example.dz78_pro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Dz78ProApplication {

    public static void main(String[] args) {
        SpringApplication.run(Dz78ProApplication.class, args);
    }
}
