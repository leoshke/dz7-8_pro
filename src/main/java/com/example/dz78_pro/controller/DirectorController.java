package com.example.dz78_pro.controller;

import com.example.dz78_pro.entity.Director;
import com.example.dz78_pro.repository.DirectorRepository;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1/directors")
@RestController
public class DirectorController {
    @Autowired
    private DirectorRepository directorRepository;

    @GetMapping("/")
    @Operation(summary = "Получить список всех директоров")
    public List<Director> getAllDirectorsList() {
        return directorRepository.findAll();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить директора по его id")
    public Director getDirectorById(@PathVariable long id) {
        return directorRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("This Director not found"));
    }

    @PutMapping("/")
    @Operation(summary = "Добавить нового директора или обновить существующего", description = "Для обновления существующего директора необходимо отправить директора в JSON формате с указанием поля id")
    public Director addOrUpdateDirector(@RequestBody Director director) {
        return directorRepository.saveAndFlush(director);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить директора по id")
    public void deleteDirector(@PathVariable long id) {
        directorRepository.deleteById(id);
    }
}
