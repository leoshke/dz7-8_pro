package com.example.dz78_pro.controller;

import com.example.dz78_pro.entity.User;
import com.example.dz78_pro.repository.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1/users")
@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    @Operation(summary = "Получить список всех пользователей", description = "Получение списка всех пользователей в БД")
    public List<User> getAllUsersList() {
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить пользователя по его id", description = "Получение записи пользователя по его идентификтатору")
    public User getUserById(@PathVariable long id) {
        return userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("This User not found"));
    }

    @PutMapping("/")
    @Operation(summary = "Добавить нового пользователя или обновить существующего",
            description = "Для обновления существующего пользователя необходимо отправить пользователя в JSON формате с указанием поля id и, " +
                    "во избежание создания дублирующей записи роли, указать id также и в роли")
    public User addOrUpdateUser(@RequestBody User user) {
        return userRepository.saveAndFlush(user);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить пользователя по id")
    public void deleteUser(@PathVariable long id) {
        userRepository.deleteById(id);
    }
}
