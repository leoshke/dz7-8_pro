package com.example.dz78_pro.controller;

import com.example.dz78_pro.entity.Film;
import com.example.dz78_pro.entity.Order;
import com.example.dz78_pro.entity.User;
import com.example.dz78_pro.repository.FilmRepository;
import com.example.dz78_pro.repository.OrderRepository;
import com.example.dz78_pro.repository.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1/orders")
@RestController
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FilmRepository filmRepository;

    @GetMapping("/")
    @Operation(summary = "Получить список всех заказов")
    public List<Order> getAllOrdersList() {
        return orderRepository.findAll();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить заказ по его id")
    public Order getOrderById(@PathVariable long id) {
        return orderRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("This Order not found"));
    }

    @PutMapping("create/users/{userId}/films/{filmId}")
    @Operation(summary = "Создать новый заказ или обновить существующий",
            description = "Принимает id пользователя, который совершает заказ и id фльма, на который совершается заказ. " +
                    "Для обновления существующего заказа необходимо отправить заказ в JSON формате с указанием поля id")
    public Order addOrUpdateOrder(@PathVariable long userId, @PathVariable long filmId, @RequestBody Order order) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("There is no user with this id"));
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new EntityNotFoundException("There is no film with this id"));
        order.setUser(user);
        order.setFilm(film);
        return orderRepository.saveAndFlush(order);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить заказ по id")
    public void deleteOrder(@PathVariable long id) {
        orderRepository.deleteById(id);
    }
}
