package com.example.dz78_pro.controller;

import com.example.dz78_pro.entity.Director;
import com.example.dz78_pro.entity.Film;
import com.example.dz78_pro.repository.DirectorRepository;
import com.example.dz78_pro.repository.FilmRepository;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1/films")
@RestController
public class FilmController {
    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private DirectorRepository directorRepository;

    @GetMapping("/")
    @Operation(summary = "Получить список всех фильмов")
    public List<Film> getAllFilmsList() {
        return filmRepository.findAll();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить фильм по его id")
    public Film getFilmById(@PathVariable long id) {
        return filmRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("This Film not found"));
    }

    @PatchMapping("/{filmId}/directors/{directorId}/assign")
    @Operation(summary = "Связать директора с фильмом")
    public void assignDirectorToFilm(@PathVariable long filmId, @PathVariable long directorId) {
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new EntityNotFoundException("There is no film with id " + filmId));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new EntityNotFoundException("There is no director with id " + directorId));
        film.addDirector(director);
        filmRepository.save(film);
    }

    @PutMapping("/")
    @Operation(summary = "Добавить новый фильм или обновить существующий", description = "Для обновления существующего фильма необходимо отправить фильм в JSON формате с указанием поля id")
    public Film addOrUpdateFilm(@RequestBody Film film) {
        return filmRepository.saveAndFlush(film);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить фильм по id")
    public void deleteFilm(@PathVariable long id) {
        filmRepository.deleteById(id);
    }
}
